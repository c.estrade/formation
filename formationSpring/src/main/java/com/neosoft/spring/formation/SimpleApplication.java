package com.neosoft.spring.formation;

import com.neosoft.spring.formation.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleApplication implements CommandLineRunner {

    @Autowired
    DocumentService documentService;

    public static void main(String[] args) {
        SpringApplication.run(SimpleApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        DocumentService documentService1 = (DocumentService) Bia.getByClass(DocumentService.class);
        DocumentService documentService2 = (DocumentService) Bia.getByClass(DocumentService.class);
        System.out.println(documentService.toString());
        System.out.println(documentService1);
        System.out.println(documentService2);
    }
}
