package com.neosoft.spring.formation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.time.LocalDateTime;

@SpringBootApplication
public class FormationApplication {

    private static final Logger log = LoggerFactory.getLogger(FormationApplication.class);

    @Bean(name = "date")
    // retourne une nouvelle instance du bean à chaque appel
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    static LocalDateTime getDateNom() {
        return LocalDateTime.now();
    }

    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(FormationApplication.class, args);

        for (int i = 0; i <= 10; i++) {
            try {
                Thread.sleep(1000);

                LocalDateTime date = context.getBean("date", LocalDateTime.class);
                log.info("la date du jour est : {}", date);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
