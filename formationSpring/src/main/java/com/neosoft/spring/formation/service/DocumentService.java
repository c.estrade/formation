package com.neosoft.spring.formation.service;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Scope("prototype")
public class DocumentService {

    @Value("${app.label.test}")
    private String test;

    @Value("${app.label.test2: zeubi}")
    private String test2;
    private static Logger LOG = LoggerFactory.getLogger(DocumentService.class);

    private LocalDateTime date;

    public DocumentService(LocalDateTime date) {
        this.date = date;
    }

    public void sayHello() {
        LOG.info("hello");
    }

    @PostConstruct
    public void init() {
        LOG.info("je debute ce service");
    }

    @PreDestroy
    public void end() {
        LOG.info("Le service vient d'etre détruit");
    }

    @Override
    public String toString() {
        return "DocumentService{" +
                "date=" + date +
                " test=" + test +
                " test2=" + test2 +
                '}';
    }
}
