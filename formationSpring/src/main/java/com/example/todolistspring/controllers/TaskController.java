package com.example.todolistspring.controllers;

import com.example.todolistspring.exception.ExceptionNOTFOUND;
import com.example.todolistspring.model.IdpToken;
import com.example.todolistspring.model.Task;
import com.example.todolistspring.services.TaskServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Base64;
import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    TaskServices taskServices;


    @ExceptionHandler(ExceptionNOTFOUND.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleGenericException(ExceptionNOTFOUND ex) {
        // Log the exception or perform other actions
        return "NOT_FOUND  : " + ex.getMessage(); // Forward to a custom error page
    }

    @GetMapping("/{id}")
    public ResponseEntity<Task> getTask(@PathVariable int id) throws Exception, ExceptionNOTFOUND {
        Task task = taskServices.getTaskPerId(id);
        if (task != null) {
            return new ResponseEntity<>(task, HttpStatus.OK);
        } else {
            throw new ExceptionNOTFOUND("aucune tache trouvé");
        }

    }

    @GetMapping("/getAllTask")
    public List<Task> getListTask() {
        return taskServices.getTaskFromApi();
    }

    @Value("${openId.credential.clientId}")
    String clientId;
    @Value("${openId.credential.secret}")
    String secret;
    @Value("${openId.token.endpoint}")
    String token_endpoint;
    @Value("${openId.token.redirect_uri}")
    String redirect_uri;
    private static final Logger LOG = LoggerFactory.getLogger(TaskController.class);


    @GetMapping("/code")
    public IdpToken code(@RequestParam String code) {
        String authorization = clientId + ":" + secret;
        String authorization64 = "basic " + Base64.getEncoder().encodeToString(authorization.getBytes());
        MultiValueMap headers = new HttpHeaders();
        headers.put("Authorization", Arrays.asList(new String[]{authorization64}));
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("grant_type", "authorization_code");
        map.add("code", code);
        map.add("redirect_uri", redirect_uri);
        HttpEntity request = new HttpEntity(map, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<IdpToken> response;
        try {
            response = restTemplate.exchange(token_endpoint, HttpMethod.POST, request, IdpToken.class);
            LOG.info("response  :\n" + response);
        } catch (HttpClientErrorException e) {
            LOG.info("Erreur : " + e.getStatusCode() + " data : " + e.getResponseBodyAsString());
            throw e;
        }
        return response.getBody();
    }


    @PostMapping("/createTask")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTask(@RequestBody Task task) {
        taskServices.addTask(task);
    }

    @PutMapping("/updateTask/{id}")
    public void updateTask(@PathVariable int id, @RequestBody Task task) {
        taskServices.updateTask(id, task);
    }


    @GetMapping("/deleteTask/{id}")
    public void deleteTask(@PathVariable int id) {
        // ....
        taskServices.deleteTask(id);
    }
}
