package com.example.todolistspring.repository;

import com.example.todolistspring.model.ListOfPerson;
import com.example.todolistspring.model.ListOfTask;
import com.example.todolistspring.model.Person;
import com.example.todolistspring.model.Task;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public class PersonRepository {

    public Optional<Person> findPersonPerId(int id) {
        return ListOfPerson.getListOfPerson().stream().filter(person -> person.getId() == id).findFirst();
    }
    public List<Person> findAll() {
        return ListOfPerson.getListOfPerson();
    }

    public void deletePersonPerId(int id) {
        ListOfPerson.getListOfPerson().remove(ListOfPerson.getListOfPerson().stream().filter(person -> person.getId() == id).findFirst().get());
    }

    public void addPerson(Person person) {
        ListOfPerson.addPerson(person);
    }

    public void updatePerson(int id, Person person) {
        // TODO check avant le if present
        ListOfPerson.getListOfPerson().stream().filter(personObj -> personObj.getId() == id).forEach(personToChange -> mergeElem(personToChange, person));
    }

    public void mergeElem(Person person1, Person person2) {
        person1.setAge(person2.getAge());
        person1.setNom(person2.getNom());
        person1.setPrenom(person2.getPrenom());
    }
}
