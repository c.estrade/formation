package com.example.todolistspring.services;

import com.example.todolistspring.model.Task;
import com.example.todolistspring.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TaskServices {

    @Autowired
    TaskRepository taskRepository;
    public Task getTaskPerId(int id) {
        Optional<Task> optionTask = taskRepository.findTaskPerId(id);
        return optionTask.isPresent()? optionTask.get(): null;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public void deleteTask(int id) {
        taskRepository.deleteTaskPerId(id);
    }

    public void addTask(Task task) {
        taskRepository.addTask(task);
    }

    public void updateTask(int id, Task task) {
        // Task task = taskRepository.findTaskPerId(id);
        taskRepository.updateTask(id, task);
    }

    public List<Task> getTaskFromApi() {
        // Task task = taskRepository.findTaskPerId(id);
        return taskRepository.getTaskFromApi();
    }

}
