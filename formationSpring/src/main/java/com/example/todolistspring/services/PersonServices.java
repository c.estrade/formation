package com.example.todolistspring.services;

import com.example.todolistspring.model.Person;
import com.example.todolistspring.model.Task;
import com.example.todolistspring.repository.PersonRepository;
import com.example.todolistspring.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonServices {


    @Autowired
    PersonRepository personRepository;
    public Person getPersonPerId(int id) {
        Optional<Person> optionPerson = personRepository.findPersonPerId(id);
        return optionPerson.isPresent()? optionPerson.get(): null;
    }

    public List<Person> findAll() {
        return personRepository.findAll();
    }

    public void deletePerson(int id) {
        personRepository.deletePersonPerId(id);
    }

    public void addPerson(Person person) {
        personRepository.addPerson(person);
    }

    public void updatePerson(int id, Person person) {
        // Person person = personRepository.findPersonPerId(id);
        personRepository.updatePerson(id, person);
    }
}
