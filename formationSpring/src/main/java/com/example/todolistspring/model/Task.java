package com.example.todolistspring.model;

import java.time.LocalDate;

public class Task {

    private int id;
    private int duree;
    private Person person;
    private LocalDate startDate;
    private LocalDate endDate;

    public Task() {
    }

/*    public Task(int duree, Person person, LocalDate startDate, LocalDate endDate) {
        this.duree = duree;
        this.person = person;
        this.startDate = startDate;
        this.endDate = endDate;
    }*/

    public Task(int duree, Person person, LocalDate startDate, LocalDate endDate) {
        this.duree = duree;
        this.person = person;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    protected void setId(int id) {
        this.id = id;
    }


    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }


    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", duree=" + duree +
                ", person=" + person +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
