package com.example.todolistspring.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ListOfPerson {
    @Getter
    private static List<Person> listOfPerson = new ArrayList<>();

    public ListOfPerson() {
    }


    public static void addPerson(Person person) {
        Optional<Person> maxTaskOption = ListOfPerson.listOfPerson.stream().max(Comparator.comparing(Person::getId));
        int maxTaskId = 0;
        if (maxTaskOption.isPresent()) {
            maxTaskId = maxTaskOption.get().getId();
        }
        person.setId(maxTaskId + 1);
        ListOfPerson.listOfPerson.add(person);
    }
}
