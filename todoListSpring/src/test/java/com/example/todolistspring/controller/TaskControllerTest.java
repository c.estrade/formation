package com.example.todolistspring.controller;

import com.example.todolistspring.controllers.TaskController;
import com.example.todolistspring.model.Person;
import com.example.todolistspring.model.Task;
import com.example.todolistspring.services.TaskServices;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = TaskController.class)
public class TaskControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TaskServices taskServices;

    @Test
    public void should_return_all_tasks_in_json() throws Exception {
        mockTaskServiceRetrunTwoTask();

        var result = mockMvc.perform(MockMvcRequestBuilders.get("/task/getAllTask")).andExpect(status().isOk());
        result.andExpectAll(jsonPath("$.length()").value(2),
                jsonPath("[0].id").value(1),
                jsonPath("[0].duree").value(3));
    }

    private void mockTaskServiceRetrunTwoTask() {
        when(taskServices.findAll()).thenReturn(List.of(new Task[]{new Task(1, 3, new Person(), LocalDate.now(), LocalDate.now()), new Task()}));
    }
}
