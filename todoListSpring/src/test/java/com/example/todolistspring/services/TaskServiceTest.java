package com.example.todolistspring.services;

import com.example.todolistspring.repository.TaskRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.mockito.Mockito.when;


@SpringBootTest
public class TaskServiceTest {
    @Autowired
    TaskServices taskServices;
    @MockBean
    TaskRepository taskRepository;

    @Test
    void getAll_should_return_all_task() {
        mockRepositoryReturnAnEmptyList();
        taskServices.findAll();
    }

    private void mockRepositoryReturnAnEmptyList() {
        when(taskRepository.findAll()).thenReturn(List.of());
    }
}
