package com.example.todolistspring.repository;

import com.example.todolistspring.model.Task;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;

@DataMongoTest
public class TaskRespositoryTest {
    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    TaskRepository taskRepository;

    @BeforeEach
    void before() {
        mongoTemplate.save(fakeTask());
    }

    @AfterEach
    void after() {
        mongoTemplate.getDb().drop();
    }

    @Test
    void getAll_should_return_all_tasks_in_repo() {
        Assertions.assertThat(taskRepository.findAll()).hasSize(1);
    }
    private Task fakeTask() {
        return new Task();
    }
}
