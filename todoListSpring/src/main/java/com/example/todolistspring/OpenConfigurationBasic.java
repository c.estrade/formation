package com.example.todolistspring;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@SecurityScheme(
        name = "basicAuthent",
        type = SecuritySchemeType.HTTP,
        scheme = "basic"
)
public class OpenConfigurationBasic {

}
