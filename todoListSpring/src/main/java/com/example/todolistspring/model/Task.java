package com.example.todolistspring.model;

import jakarta.persistence.*;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;


/*@Table(name = "task")
@Entity(name = "task")*/
@Document("task")
public class Task {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    //@Column(name = "duree")
    @Indexed
    private int duree;

    // @ManyToOne
    // @JoinColumn(name = "id_person")
    private Person person;
    // @Column(name = "startDate")
    private LocalDate startDate;
    // @Column(name = "endDate")
    private LocalDate endDate;

    public Task() {
    }

    public Task(int id, int duree, Person person, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.duree = duree;
        this.person = person;
        this.startDate = startDate;
        this.endDate = endDate;
    }
/*    public Task(int duree, Person person, LocalDate startDate, LocalDate endDate) {
        this.duree = duree;
        this.person = person;
        this.startDate = startDate;
        this.endDate = endDate;
    }*/

    public Task(int duree, Person person, LocalDate startDate, LocalDate endDate) {
        this.duree = duree;
        this.person = person;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    protected void setId(int id) {
        this.id = id;
    }


    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }


    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", duree=" + duree +
                ", person=" + person +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
