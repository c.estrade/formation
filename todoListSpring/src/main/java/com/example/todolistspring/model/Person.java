package com.example.todolistspring.model;


import jakarta.persistence.*;

/*
@Table(name = "person")
@Entity(name = "person")
*/
public class Person {

    @Id
//     @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    //   @Column(name = "nom")
    private String nom;

    //  @Column(name = "prenom")
    private String prenom;

    //     @Column(name = "age")
    private int age;

    public Person() {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person(int id, String nom, String prenom, int age) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", age=" + age +
                '}';
    }
}
