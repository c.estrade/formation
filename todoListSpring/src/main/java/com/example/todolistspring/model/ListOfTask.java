package com.example.todolistspring.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ListOfTask {
    @Getter
    private static List<Task> listOfTask = new ArrayList<>();

    public ListOfTask() {
    }


    public static void addTask(Task task) {
        Optional<Task> maxTaskOption = ListOfTask.listOfTask.stream().max(Comparator.comparing(Task::getId));
        int maxTaskId = 0;
        if (maxTaskOption.isPresent()) {
            maxTaskId = maxTaskOption.get().getId();
        }
        task.setId(maxTaskId + 1);
        ListOfTask.listOfTask.add(task);
    }
}
