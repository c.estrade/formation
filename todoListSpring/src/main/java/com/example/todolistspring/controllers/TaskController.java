package com.example.todolistspring.controllers;

import com.example.todolistspring.exception.ExceptionNOTFOUND;
import com.example.todolistspring.model.Task;
import com.example.todolistspring.services.SecurityServices;
import com.example.todolistspring.services.TaskServices;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;/*
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.security.core.context.SecurityContext;*/
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
@SecurityRequirement(name = "basicAuthent")
public class TaskController {

    @Autowired
    TaskServices taskServices;


    @ExceptionHandler(ExceptionNOTFOUND.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleGenericException(ExceptionNOTFOUND ex) {
        // Log the exception or perform other actions
        return "NOT_FOUND  : " + ex.getMessage(); // Forward to a custom error page
    }

    @GetMapping("/{id}")
    public ResponseEntity<Task> getTask(@PathVariable int id) throws Exception, ExceptionNOTFOUND {
        Task task = taskServices.getTaskPerId(id);
        if (task != null) {
            return new ResponseEntity<>(task, HttpStatus.OK);
        } else {
            throw new ExceptionNOTFOUND("aucune tache trouvé");
        }

    }
/*    @GetMapping("/who")
    public String method(@CurrentSecurityContext SecurityContext context) {
        return context.getAuthentication().getName();
    }*/


    @GetMapping("/getAllTask")
    public List<Task> getListTask() {
        //SecurityServices.checkSecurity();
        return taskServices.findAll();
    }

    @PostMapping("/createTask")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTask(@RequestBody Task task) {
        taskServices.addTask(task);
    }

    @PutMapping("/updateTask/{id}")
    public void updateTask(@PathVariable int id, @RequestBody Task task) {
        taskServices.updateTask(id, task);
    }


    @GetMapping("/deleteTask/{id}")
    public void deleteTask(@PathVariable int id) {
        // ....
        taskServices.deleteTask(id);
    }
}
