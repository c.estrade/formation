package com.example.todolistspring.controllers;


import com.example.todolistspring.model.Person;
import com.example.todolistspring.services.MessageSender;
import com.example.todolistspring.services.PersonServices;
import jakarta.jms.JMSException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/personn")
public class PersonController {

    @Autowired
    PersonServices personServices;

    @Autowired
    MessageSender messageSender;

    @GetMapping("/{id}")
    public Person getPerson(@PathVariable int id) throws Exception {
        Person person = personServices.getPersonPerId(id);
        if (person != null) {
            return person;
        } else {
            throw new Exception("aucune tache trouvé");
        }

    }

    @GetMapping("/getAllPerson")
    public List<Person> getListPerson() {
        return personServices.findAll();
    }

    @PostMapping("/createPerson")
    @ResponseStatus(HttpStatus.CREATED)
    public void createPerson(@RequestBody Person person) {
        personServices.addPerson(person);
    }

    @PutMapping("/updatePerson/{id}")
    public void updatePerson(@PathVariable int id, @RequestBody Person person) {
        personServices.updatePerson(id, person);
    }

    @GetMapping("/sendmsg")
    public void sendMsg() {
        messageSender.sendMessage("je suis un message");
    }
    @GetMapping("/getmsg")
    public void getMsg() throws JMSException {
        messageSender.getMessage();
    }


    @GetMapping("/deletePerson/{id}")
    public void deletePerson(@PathVariable int id) {
        // ....
        personServices.deletePerson(id);
    }
}
