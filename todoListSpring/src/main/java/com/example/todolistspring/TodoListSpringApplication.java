package com.example.todolistspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
public class TodoListSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodoListSpringApplication.class, args);
    }

}
