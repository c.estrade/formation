package com.example.todolistspring.repository;

import com.example.todolistspring.model.ListOfTask;
import com.example.todolistspring.model.Person;
import com.example.todolistspring.model.Task;
import jakarta.annotation.PostConstruct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends MongoRepository<Task, Integer> {
    Task findById(int id);

   /* @PostConstruct
    public void createLotOfTask() {

        Person person = new Person(1, "estrade", "prenom", 24);
        Task task = new Task(3, person, LocalDate.now(), LocalDate.now());
        Task task1 = new Task(3, person, LocalDate.now(), LocalDate.now());
        Task task2 = new Task(3, person, LocalDate.now(), LocalDate.now());
        ListOfTask.addTask(task);
        ListOfTask.addTask(task1);
        ListOfTask.addTask(task2);
    }

    public Optional<Task> findTaskPerId(int id) {
        return ListOfTask.getListOfTask().stream().filter(task -> task.getId() == id).findFirst();
    }
    public List<Task> findAll() {
        return ListOfTask.getListOfTask();
    }

    public void deleteTaskPerId(int id) {
        ListOfTask.getListOfTask().remove(ListOfTask.getListOfTask().stream().filter(task -> task.getId() == id).findFirst().get());
    }

    public void addTask(Task task) {
        ListOfTask.addTask(task);
    }

    public void updateTask(int id, Task task) {
        // TODO check avant le if present
        ListOfTask.getListOfTask().stream().filter(taskObj -> taskObj.getId() == id).forEach(taskToChange -> mergeElem(taskToChange, task));
    }

    public void mergeElem(Task task1, Task task2) {
        task1.setDuree(task2.getDuree());
        task1.setStartDate(task2.getStartDate());
        task1.setEndDate(task2.getEndDate());
        task1.setPerson(task2.getPerson());
    }*/
}
