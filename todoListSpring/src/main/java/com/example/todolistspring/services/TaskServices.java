package com.example.todolistspring.services;

import com.example.todolistspring.aspect.MonAnnot;
import com.example.todolistspring.model.Task;
import com.example.todolistspring.repository.PersonRepository;
import com.example.todolistspring.repository.TaskRepository;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskServices {

    @Autowired
    TaskRepository taskRepository;
    @Autowired
    PersonRepository personRepository;

    private final MeterRegistry meterRegistry;
    public Task getTaskPerId(int id) {
        return taskRepository.findById(id);
    }

    @MonAnnot
    public List<Task> findAll() {
        var counter = meterRegistry.counter("taskservice.getall.invoked");
        counter.increment();
        return taskRepository.findAll();
    }

    public void deleteTask(int id) {
        taskRepository.deleteById(id);
        //taskRepository.deleteTaskPerId(id);
    }

    public void addTask(Task task) {
        taskRepository.insert(task);
        //personRepository.saveAndFlush(task.getPerson());
        //taskRepository.saveAndFlush(task);
        //taskRepository.addTask(task);
    }

    public void updateTask(int id, Task task) {
        Task taskToChange = taskRepository.findById(id);
        mergeElem(taskToChange, task);
        //taskRepository.saveAndFlush(taskToChange);
        // taskRepository.updateTask(id, task);
    }

    private void mergeElem(Task task1, Task task2) {
        task1.setDuree(task2.getDuree());
        task1.setStartDate(task2.getStartDate());
        task1.setEndDate(task2.getEndDate());
        task1.setPerson(task2.getPerson());
    }
}
