package com.example.todolistspring.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class MessageProducer {
    private final KafkaTemplate<String, String> template;

    @Value("${input-topic}")
    private String topicName;
    public void sendMessage(String msg) {
        template.send(topicName, msg).whenComplete((t, v) -> log.info(String.valueOf(t)));
    }
}
