package com.example.todolistspring.services;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class MessageSender {
    private final JmsTemplate template;

    @Value("${input-queue}")
    private String destination;

    public void sendMessage(String message) {
        template.convertAndSend(destination, message);
    }

    public void getMessage() throws JMSException {
        Message test = template.receive(destination);
        log.info("mon message : {}", test.getJMSMessageID());
    }

}
