package com.example.todolistspring.listener;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.TextMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class QueueListener {
    @JmsListener(destination = "${input-queue}")
    public void onMessageReceived(Message message) throws JMSException {
        if (message instanceof TextMessage textMessage) {
            String text = textMessage.getText();
            log.info(text);
        }
    }
}
