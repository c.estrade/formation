package com.example.todolistspring.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class PerfLogAspect {

    @Around("execution(public * com.example.todolistspring.services.TaskServices.*(..))")
    public Object log(ProceedingJoinPoint pjp) throws Throwable {
        var timeBefore = System.currentTimeMillis();

        var result = pjp.proceed();

        var timeAfter = System.currentTimeMillis();

        var executedTime = timeAfter - timeBefore;
        log.info("Elapsed time in method {} : {} ms", pjp.getSignature().getName(), executedTime);
        log.info("Signature is {} and parameter are {}", pjp.getSignature().getName(), pjp.getArgs());
        if (executedTime > 2000) {
            log.info("Le temps d'execution de la méthode est trop long");
        }
        return result;
    }


    @Around("@annotation(MonAnnot)")
    public Object logAnnot(ProceedingJoinPoint pjp) throws Throwable {
        var timeBefore = System.currentTimeMillis();

        var result = pjp.proceed();

        var timeAfter = System.currentTimeMillis();

        var executedTime = timeAfter - timeBefore;
        log.info("Elapsed time in method {} : {} ms", pjp.getSignature().getName(), executedTime);
        log.info("Signature is {} and parameter are {}", pjp.getSignature().getName(), pjp.getArgs());
        if (executedTime > 2000) {
            log.info("Le temps d'execution de la méthode est trop long");
        }
        return result;
    }
}
