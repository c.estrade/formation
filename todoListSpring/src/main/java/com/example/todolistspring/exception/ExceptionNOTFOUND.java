package com.example.todolistspring.exception;

public class ExceptionNOTFOUND extends Throwable {

    public ExceptionNOTFOUND(String message) {
        super(message);
    }
}
